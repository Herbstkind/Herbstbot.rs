extern crate irc;

use irc::client::prelude::{ClientExt, Command, Config, IrcClient, IrcReactor};
use irc::error::Result;
use irc::proto::Message;

fn main() {

    let cfg = Config {
        nickname: Some(format!("Herbstbot")),
        server: Some(format!("irc.rizon.net")),
        channels: Some(vec![format!("#Herbstkanal")]),
        ..Config::default()
    };

    // Instanciate and connect
    let mut reactor = IrcReactor::new().unwrap();
    let client = reactor.prepare_client_and_connect(&cfg).unwrap();
    client.identify().unwrap();

    // register handler and run
    reactor.register_client_with_handler(client, &message_handler);
    reactor.run().unwrap();
}

fn message_handler(client: &IrcClient, msg: Message) -> Result<()> {
    // TODO Use logging instead of println. How does the irc crate log?
    println!("<<< {}", msg);

    match msg.command {
        // TODO Why is everything a PRIVMSG...

        // Join all channels we're invited to
        Command::INVITE(_, chan) => client.send_join(&chan),

        // TODO
        Command::VERSION(_) => {
            println!("I GOT A VERSION MESSAGE WAAAAAH!!!!!!!!!!! {}", msg);
            Ok(())
        },

        //TODO react to private messages
        Command::PRIVMSG(_, msg) => {
            println!("I GOT A MESSAGE GEKRIEGT WAAAAAH!!!!!!!!!!! {}", msg);
            client.send_join("#Herbstkanal2")
        },

        // Ignore the rest silently
        _ => Ok(()),
    }

}
