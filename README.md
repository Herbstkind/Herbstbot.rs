# Herbstbot - IRC bot by Herbstkind

Random IRC bot written in rust because I felt like it.


## What it can do
- Join any channel it is invited to


## ToDo

- Identify me on the IRC by my login ("is logged in as")
- Identify me on the IRC by password, vhost, or whatever authentication token supplied at runtime
- Shell arg for network, nick, channels, Operator (me, see above)
- Govern a channel democratically
